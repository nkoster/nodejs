'use strict';

const
    cluster = require('cluster'),
    cpus = require('os').cpus().length;

if (cluster.isWorker) {
    process.on('message', (msg) =>
        console.log(`Worker ${process.pid} received message from master:`, msg));
}

if (cluster.isMaster) {
    console.log(`Master ${process.pid} has started.`);
    for (let i = 0; i < cpus; i++) {
        let worker = cluster.fork();
        worker.send({msgFromMaster: `This is from master ${process.pid} to worker ${worker.process.pid}.`});
    }
    cluster.on('death', (worker) =>
        console.log(`Worker ${worker.pid} died.`));
}