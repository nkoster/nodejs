// object properties and methods

var obj = {
    greet: 'Hello'
};

console.log(obj.greet);
console.log(obj['greet']);

var prop = 'greet';
console.log(obj[prop]);

// functions and arrays

var arr = [];
arr.push(function () {
    console.log('hello word I')
});
arr.push(function () {
    console.log('hello word II')
});
arr.push(function () {
    console.log('hello word III')
});

arr.forEach(function (item) {
    item()
});
