var Person = {
    firstname: '',
    lastname: '',
    full: function () {
        return this.firstname + ' ' + this.lastname
    }
};

var niels = Object.create(Person);
niels.firstname = 'Niels';
niels.lastname = 'Koster';

var laury = Object.create(Person);
laury.firstname = 'Laury';
laury.lastname = 'Koster';

console.log(niels.full() + ' ' + laury.full());

function speak(line) {
    console.log('the ' + this.type + ' rabbit says "' + line + '"')
}
speak.call({ type: 'old' }, 'Oh my!' );
speak.call({ type: 'new' }, 'shit!' );
