'use strict';

const
    fs = require('fs'),
    readable = fs.createReadStream(__dirname  + '/aaap.txt', { encoding:'utf8', highWaterMark:500 }),
    writable = fs.createWriteStream(__dirname + '/aaap-copy.txt');

let
    count = 0,
    total = 0;

readable.on('data', function (chunk) {
    count++;
    total += chunk.length;
    console.log(`count=${count} chunk=${chunk.length} total=${total}`);
    writable.write(chunk)
});
