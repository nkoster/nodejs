'use strict';

class Person {
    constructor(f, n) {
        this.firstname = f;
        this.lastname = n
    }
    greet() {
        console.log('Hello ' + this.firstname + ' ' + this.lastname)
    }
}

var niels = new Person('Niels', 'Koster');
niels.greet();
