var obj = {
    name: 'John Doe',
    greet: function(data) {
        console.log(`Hello ${ this.name } ${data}`)
    }
};

var data = 'apekop';

obj.greet();
obj.greet.call({ name: 'Jane Doe' }, data);
obj.greet.apply({ name: 'Jane Doe' }, []);
