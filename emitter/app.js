//var Emitter = require('./emitter');
var Emitter = require('events');
var eventConfig = require('./config').events;

var emtr = new Emitter();

emtr.on(eventConfig.GREET, function () {
    console.log('Someone, somewhere, said hello')
});

emtr.on(eventConfig.GREET, function () {
    console.log('Another greeting occured')
});

console.log('Hi!');
emtr.emit(eventConfig.GREET);
