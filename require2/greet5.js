const greeting = 'Hello universe';

function greet() {
    console.log(greeting)
}

module.exports = {
    greet: greet
};
