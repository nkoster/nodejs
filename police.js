var util = require('util');

function Person(f,n) {
    this.firstname = f,
    this.lastname = n
}

Person.prototype.greet = function() {
    console.log('Hello ' + this.firstname + ' ' + this.lastname)
}

function Policeman(f,n) {
    Person.call(this, f,n);
    this.badgenumber = '123456'
}

util.inherits(Policeman, Person);

var officer1 = new Policeman('Niels', 'Koster');
officer1.greet();

var officer2 = new Policeman('Laury', 'Koster');
officer2.greet();
