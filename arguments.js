function doSomething(f) {
    return f()
}

console.log(doSomething(function () {
    console.log(this)
}));

