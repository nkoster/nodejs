const util = require('util');

const name = 'Niels';
const greeting = util.format('Hello %s', name);

util.log(greeting);
