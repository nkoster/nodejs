'use strict';

const
    express = require('express'),
    bodyParser = require('body-parser'),
    urlencodedParser = bodyParser.urlencoded({ extended: false }),
    jsonParser = bodyParser.json(),
    app = express(),
    // take a port from the environment or default to 3000
    port = process.env.PORT || 3000;

// template engine
app.set('view engine', 'ejs');

// static files middleware
app.use('/public', express.static(__dirname + '/static'));

// custom middleware
app.use('/', function (req, res, next) {
    console.log('Request URL: ' + req.url);
    next()
});

// index page from /
app.get('/', function (req, res) {
    // render template index.ejs
    res.render('index')
});

// pass exact two variables via the url
app.get('/person/:firstname/:lastname', function (req, res) {
    res.render('person', { firstname: req.params.firstname, lastname: req.params.lastname })
});

// pass a query string ?noot=mies
app.get('/id/:id', function (req, res) {
    res.render('id', { id: req.params.id, noot: req.query.noot })
});

// post with middleware in between
app.post('/id', urlencodedParser, function (req, res) {
    res.send('Thank you!');
    console.log(req.body.firstname);
    console.log(req.body.lastname)
});

// post with json middleware in between
app.post('/id-person', jsonParser, function (req, res) {
    res.send('Thank you for the JSON data!')
    console.log(req.body.firstname);
    console.log(req.body.lastname)
});

// JSON response


app.listen(port);
