const
    bodyParser = require('body-parser'),
    jsonParser = bodyParser.json(),
    urlencodedParser = bodyParser.urlencoded({ extended: false });

    module.exports = function (app) {

// index page from /
    app.get('/', function (req, res) {
        // render template index.ejs
        res.render('index')
    });

// pass exact two variables via the url
    app.get('/person/:firstname/:lastname', function (req, res) {
        res.render('person', { firstname: req.params.firstname, lastname: req.params.lastname })
    });

// pass a query string ?noot=mies
    app.get('/id/:id', function (req, res) {
        res.render('id', { id: req.params.id, noot: req.query.noot })
    });

// post with middleware in between
    app.post('/id', urlencodedParser, function (req, res) {
        res.send('Thank you!');
        console.log(req.body.firstname);
        console.log(req.body.lastname)
    });

// post with json middleware in between
    app.post('/id-person', jsonParser, function (req, res) {
        res.send('Thank you for the JSON data!')
        console.log(req.body.firstname);
        console.log(req.body.lastname)
    });

// JSON response
    app.get('/api', function (req, res) {
        res.json({ firstname: 'Niels', lastname: 'Koster' })
    });

};
