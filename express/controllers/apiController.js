const
    bodyParser = require('body-parser'),
    jsonParser = bodyParser.json();

module.exports = function (app) {

    app.get('/api/person/:id', function (req, res) {
        // get data from database
    });

    app.post('/api/', jsonParser, function (req, res) {
        // save data to the database
    });

    app.delete('/api/person/:id', function (req, res) {
        // delete from the database
    })
};
