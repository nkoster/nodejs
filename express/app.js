'use strict';

const
    express = require('express'),
    app = express(),
    // my request and response handlers
    apiController = require('./controllers/apiController'),
    htmlController = require('./controllers/htmlController'),
    // take a port from the environment or default to 3000
    port = process.env.PORT || 3000;

// template engine
app.set('view engine', 'ejs');

// static files middleware
app.use('/public', express.static(__dirname + '/static'));

// custom middleware
app.use('/', function (req, res, next) {
    console.log('Request URL: ' + req.url);
    next()
});

// the API is abstracted away into ./controllers
apiController(app);
// the HTML as well
htmlController(app);

app.listen(port);
