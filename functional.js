function greaterThan(n) {
    return function(m) { return m > n }
}

var greaterThan10 = greaterThan(10);
console.log(greaterThan10(9));
console.log(greaterThan(10)(11));

function reduce(arr, combine, start) {
    var current = start;
    for (var i = 0; i < arr.length; i++) {
        current = combine(current, arr[i])
    }
    return current
}

console.log(reduce([1,2,3,4,5,6,7,8,9], function(a, b) {
    return a + b
}, 0));

// recursive walk
function walk(item) {
    if (item >= 0) {
        console.log(item);
        return walk(item - 1)
    }
}
walk([1,2,3,4]);
