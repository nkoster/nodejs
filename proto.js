function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname
}

Person.prototype.greet = function () {
    console.log('Hello ' + this.firstname + ' ' + this.lastname)
}

var niels = new Person('Niels', 'Koster');
var aap = new Person('Ape', 'Kop');

niels.greet();
aap.greet();

console.log(niels.__proto__);
console.log(aap.__proto__);
console.log(niels.__proto__ === aap.__proto__);
