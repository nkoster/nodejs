'use strict';

const
    arr = [[1,2,3],[4,5],6,[7,8,9],10];

function flatten(a) {
    let t = [];
    const flat = function (item) {
        if (item.constructor === Array) {
            for (let i = 0; i < item.length; i++) flat(item[i])
        } else {
            t.push(item)
        }
    };
    flat(a);
    return t
}

console.log(flatten(arr));
