'use strict';

const
    fs = require('fs'),
    zlib = require('zlib'),
    gzip = zlib.createGzip(),
    readable = fs.createReadStream(__dirname  + '/aaap.txt'),
    writable = fs.createWriteStream(__dirname + '/aaap-copy.txt'),
    compressed = fs.createWriteStream(__dirname + '/aaap-copy.txt.gz');

readable.pipe(writable);
readable.pipe(gzip).pipe(compressed);
