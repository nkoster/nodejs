'use strict';

const
    log = true,
    net = require('net'),
    cluster = require('cluster'),
    cpus = require('os').cpus().length;

let workers = [];

let port_telnet = 9501;
if (process.argv.indexOf("-telnet") !== -1) {
    port_telnet = process.argv[process.argv.indexOf("-telnet") + 1];
}

let host = '127.0.0.1';
if (process.argv.indexOf("-host") !== -1) {
    host = process.argv[process.argv.indexOf("-h") + 1];
}

if (cluster.isMaster) {
    console.log(`Master ${process.pid} has started.`);
    const broadcast = (data) => {
        for (let i in workers) {
            let worker = workers[i];
            worker.send(data);
        }
    };
    for (let i = 0; i < cpus; i++) {
        let worker = cluster.fork();
        worker.send({data: `This is from master ${process.pid} to worker ${worker.process.pid}.`});
        worker.on('message', function (data) {
            console.log(`Master ${process.pid} received message from worker ${this.process.pid}.`, data);
            broadcast(data);
        });
        workers.push(worker)
    }
    cluster.on('death', (worker) =>
        console.log(`Worker ${worker.pid} died.`));
}

if (cluster.isWorker) {
    const telnetServer = net.createServer((sock) => {
        if (log) console.log(`${(new Date())} telnet connection ${sock.remoteAddress}`);
        sock.on('data', (data) => {
            // Send to master
            process.send({data: `${data}`});
        });
        sock.on('close', () => {
            if (log) console.log(`${(new Date())} closed ${sock.remoteAddress}`);
        });
    });
    telnetServer.listen(port_telnet, () =>
        console.log(`${(new Date())} telnet server started at ${host}:${port_telnet}`));
    process.on('message', (data) =>
        console.log(`Worker ${process.pid} received message from master:`, data))
}
